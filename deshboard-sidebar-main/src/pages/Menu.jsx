import React from "react"
import Nav from 'react-bootstrap/Nav'
import Navbar from 'react-bootstrap/Navbar'
import NavDropdown from 'react-bootstrap/NavDropdown'
import Container from 'react-bootstrap/Container'
//import Orden from './Orden.jsx'

export default function menu({usuario}){
       
    return (

        <Navbar bg="info" variant="light" expand="lg"  >
        <Container >
            
            <Navbar.Brand href="#home">Módulo Calificaciones</Navbar.Brand>
            <Navbar.Toggle aria-controls="basic-navbar-nav" />
            <Navbar.Collapse id="basic-navbar-nav" >
                <Nav className="me-auto ms-auto ">
{/*                     <Nav.Link href="/">Inicio</Nav.Link>
                    { usuario.funcion === "Supervisor" &&
                    <NavDropdown title="Solicitud de Trabajo" id="basic-nav-dropdown">
                        <NavDropdown.Item href="/nuevaOrden">Nueva</NavDropdown.Item>

                        <NavDropdown.Item href="/todas">Todas</NavDropdown.Item>
                        <NavDropdown.Item href="/nuevas">Sin iniciar</NavDropdown.Item>
                        <NavDropdown.Item href="/completadas">Completadas</NavDropdown.Item>
                        <NavDropdown.Item href="/cerradas">Finalizadas</NavDropdown.Item>
                    </NavDropdown>
                    }
                    { usuario.funcion === "Limpieza" &&
                        <NavDropdown title="Solicitudes" id="basic-nav-dropdown">
                            <NavDropdown.Item href="/recibidas">Solicitudes recibidas</NavDropdown.Item>
                            <NavDropdown.Item href="/sinsolicitud">Trabajo sin solicitud</NavDropdown.Item>                            
                        </NavDropdown>
                    }        */}
                </Nav>
            </Navbar.Collapse>
            <Nav className="me-auto ms-auto ">
                <NavDropdown title="👤 Cuenta" id ="basic-nav-dropdown" >
                    <NavDropdown.Item href="/">👤 {usuario.nombre}</NavDropdown.Item>
                    <NavDropdown.Item href="/logout">🔄 Cerrar sesión</NavDropdown.Item>
                </NavDropdown>
            </Nav>
        </Container>
        
        </Navbar>
    )
}