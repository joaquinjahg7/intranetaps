import { useEffect } from "react"
import { useState } from "react"
import { Form } from "react-bootstrap"
import {Col, Row,Button} from "react-bootstrap" 
import { useForm } from "react-hook-form"

const Formulario = ({value,criterios, cargarNota}) => {
    const [nota,setNota]=useState({})
    const nuevaNota = {
        idCriterio:value,
        criterio: criterios,
        nota: 0
    }    

    const handleCheckboxChange = (e) => {
        setNota({...nuevaNota,
            nota:e.target.value})     
    }
    useEffect( () => {
        console.log("revisar nota enviada:",nota)
        if (Object.entries(nota).length > 0)
            cargarNota(nota)
    },[nota])

    return(                
                <Form>
                    <Row>
                     <Col>            
                    <Form.Label column sm={12}>
                        {criterios}
                    </Form.Label>
                    </Col>
                    <Col sm>
                    {['radio'].map((type) => (
                        
                        <div key={`inline-${type}`} className="mb-3" >
                        
                        <Form.Check 
                            inline
                            label="1"
                            value="1"
                            name="group1"
                            type={type}
                            onChange= {handleCheckboxChange}

                            id={`inline-${type}-1`}
                        />
                        <Form.Check 

                            inline
                            label="2"
                            value="2"
                            name="group1"
                            type={type}
                            onChange= {handleCheckboxChange}
                            id={`inline-${type}-2`}
                        />
                        <Form.Check 
                            inline
                            label="3"
                            value="3"
                            name="group1"
                            type={type}
                            onChange= {handleCheckboxChange}

                            id={`inline-${type}-1`}
                        />
                        <Form.Check 
                            inline
                            label="4"
                            value="4"
                            name="group1"
                            type={type}
                            onChange= {handleCheckboxChange}

                            id={`inline-${type}-1`}
                        />
                        <Form.Check 
                            inline
                            label="5"
                            value="5"
                            name="group1"
                            type={type}
                            onChange= {handleCheckboxChange}

                            id={`inline-${type}-5`}
                        />
                        <Form.Check
                            inline
                            label="6"
                            value="6"
                            name="group1"
                            type={type}
                            onChange= {handleCheckboxChange}

                            id={`inline-${type}-6`}
                        /> 
                        <Form.Check
                            inline
                            label="7"
                            value="7"
                            name="group1"
                            type={type}
                            onChange= {handleCheckboxChange}

                            id={`inline-${type}-7`}
                        />
                        </div>
                    ))}</Col>
                    </Row>
                </Form>

        )
}
export default Formulario