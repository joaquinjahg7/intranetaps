import '..'
import React from 'react';
import { Form } from "react-bootstrap"
import {Button,Col, Row} from "react-bootstrap"
import Formulario from './Notas'
import { useForm } from "react-hook-form"
import "../css/bootstrap.min.css";
import ListaCategorias from './ListaCategorias'
import ListaConducta from './ListaConducta';
import ListaDesem from './ListaDesem';
import Card from 'react-bootstrap/Card'

const Auto = ({usuario, evaluacion}) => {

    return(
        <div className='container'>
            <br></br>
            <div className="row vh-100 justify-content-center align-items-center">
            <Card>
                <Card.Body>
                FACTORES DE CALIFICACIÓN:
                    7. SOBRESALIENTE/SIEMPRE
                    6. MUY BUENO/GENERALMENTE
                    5. BUENO/A MENUDO     
                    4. REGULAR/A VECES
                    3. MENOS QUE REGULAR/OCASIONALMENTE
                    2. DEFICIENTE/CASI NUNCA
                    1.  MALO/NUNCA
                </Card.Body>
            </Card>

                <ListaCategorias usuario={usuario}></ListaCategorias>

                

                <br></br>
                    <Row>
                            <Col>
                            <Form.Group className="mb-3" controlId="exampleForm.ControlTextarea1">
                                <Form.Label>Observaciones</Form.Label>
                                <Form.Control as="textarea" rows={3}  />
                            </Form.Group>
                            </Col>
                    </Row>
                    <div>
                    <Row sm>
                            
                            <Button type="submit">Enviar</Button>
                    </Row>
                    </div>
                    <br></br>
            </div>
        </div>
    )
}
export default Auto; 