import React, {Component, useState, useEffect} from 'react'
import axios from 'axios'
import { Form } from 'react-bootstrap';
import Container from 'react-bootstrap/Container'
import {Row, Col} from 'react-bootstrap'
import Formulario from './Notas'


const ListaConducta = ({usuario}) =>{

    const [categoria, setCategoria] = useState([])
    const [notas, setNotas]= useState({})
    const [evaluacion, setEvaluacion]=useState({})
    

    const cargarNota = (notas) =>{
       // setEvaluacion (...evaluacion,notas)
    }




    const getCategoria = async() => {
        await axios.get("http://localhost/ws-login/obtenerconducta.php")
        .then(response=>{
           // console.log(response.data);
            setCategoria(response.data)
        })
        .catch((error) =>{
            console.log(error);
        });
    }
    useEffect(()=>{
        getCategoria();

        setEvaluacion({
            usuario:usuario, 
            notas:notas
        })
        
    },[])
    
    return (
        <Container>
            <Form>
            <Row>
                <Col sm>
                    <Form.Label></Form.Label>
                    </Col>
                    <Col sm>
                        {categoria.map(elemento=>(                                                       
                            <Formulario key={elemento.id} value={elemento.id} criterios= {elemento.nombre} setNotas={setNotas}  cargarNota={cargarNota} >{elemento.categoria}</Formulario>
                        ))}
                    </Col>
            </Row>
            </Form>
        </Container>

  );
  }
export default ListaConducta ;
