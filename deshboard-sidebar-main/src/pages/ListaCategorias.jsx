import React, {Component, useState, useEffect} from 'react'
import axios from 'axios'
import { Form } from 'react-bootstrap';
import Container from 'react-bootstrap/Container'
import {Row, Col} from 'react-bootstrap'
import Formulario from './Notas'


const ListaCategorias = ({usuario}) =>{

    const [categoria, setCategoria] = useState([])
    const [notas, setNotas]= useState([])
    const [evaluacion, setEvaluacion]=useState({})
    

    const cargarNota = (nota) =>{//recibimos las notas y actualizamos en nuestro arreglo de notas
        let newNotas = notas
        console.log("la nota que llega",nota)

            const elementIndex = newNotas.findIndex((obj => obj.idCriterio == nota.idCriterio))
            //console.log("elemento index",elementIndex)
            if (elementIndex>=0){    //si el criterio ya estaba evaluado solo actualizar notas
                newNotas[elementIndex].nota = nota.nota
                setNotas(newNotas)
            }
            else{ // nuevo criterio evaluado
                if (Object.entries(nota).length != 0) 
                    setNotas((newNota)=>[...newNota,nota])
            }
       
        console.log("notas son:",notas)
        return 
    }
    
    const getCategoria = async() => {
        await axios.get("http://localhost/ws-login/obtenercategorias.php")
        .then(response=>{
            setCategoria(response.data)
        })
        .catch((error) =>{
            console.log(error);
        });
    }
    useEffect(()=>{// Se actualizan las notas y las usamos para actualizar las evaluaciones de cada Funcionario
        console.log("Notas actualizadas",notas)
        setEvaluacion((newEvaluacion)=>[{...newEvaluacion,notas}])
    },[notas])

    useEffect(()=>{
        console.log("la nueva evaluación:",evaluacion)//mostramos la evaluación actualizada por consola para revisar.
        // Si quieres enviar las notas al formulario autoevaluación, desde acá se debe llamar a la función que actualice los datos antes de enviarlos al backend
    },[evaluacion])
    
    useEffect(()=>{
        getCategoria();
        setEvaluacion({
            usuario:usuario, 
            
        })



    },[])
    
    return (
        
    
        <Container>
            <Form>
                <Row >
                    <Col sm={12}>
                        <Form.Label>Categoria:  </Form.Label>
                        </Col>
                        <Col sm={12}>
                            {categoria.map(elemento=>(                                                       
                                <Formulario key={elemento.id} value={elemento.id} criterios= {elemento.nombre} cargarNota={cargarNota} >{elemento.categoria}</Formulario>
                            ))}
                        </Col>
                </Row>
            </Form>
        </Container>

  );
  }
export default ListaCategorias ;
