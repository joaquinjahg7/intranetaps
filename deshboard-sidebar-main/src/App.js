import React, {useState, useEffect} from "react";
import Login from "./components/Login";
import './App.css';
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';
import Sidebar from './components/Sidebar';
import Dashboard from './pages/Dashboard.jsx';
import Analytics from './pages/Analytics.jsx';
import Comment from './pages/Comment.jsx';
import Product from './pages/Product.jsx';
import ProductList from './pages/ProductList.jsx';
import Auto  from './pages/Autoevaluacion.jsx';
import Menu from "./pages/Menu";
import Logout from "./components/Logout";
import {Row, Col} from 'react-bootstrap'


const App = () => {
  const [conectado, setConectado] = useState (false);
  const [userLogged,setUserLogged]=useState({
    nombre:"",
    apellidos:"",
    id:"",
    idTipoUsuario:"",
    jefe:"",
    etiqueTipoUsuario:"",
    usuario:""
  })
  const acceder = (estado)=> {
    setConectado(estado)
  }
  useEffect( ()=>{
    const loggedUserJson=window.localStorage.getItem('loggedUser')
    if (loggedUserJson){
        const user = JSON.parse(loggedUserJson);
        setUserLogged({
          nombre:user.nombre,
          apellidos:user.apellidos,
          id:user.id,
          idTipoUsuario:user.idTipoUsuario,
          jefe:user.jefe,
          etiquetaTipoUsuario:user.etiquetaTipoUsuario,
          usuario:user.usuario


        })
        acceder(user.conectado) 
    }

  },[])

if (conectado){
  return (
    <>
    <Row>
    <Menu usuario={userLogged}> </Menu>
    <Router>
      <Sidebar usuario={userLogged}>
        <Routes>
          <Route path="/" element={<Dashboard  usuario={userLogged}  />} />
          <Route path="/Perfil" element={<Dashboard />} />
          <Route path="/Autoevaluacion" element={<Auto usuario={userLogged.usuario} />} />
          <Route path="/Precalificacion" element={<Auto usuario={userLogged.usuario} />} />
          <Route path="/comment" element={<Comment />} />
          <Route path="/analytics" element={<Analytics />} />
          <Route path="/product" element={<Product />} />
          <Route path="/productList" element={<ProductList />} />
          <Route path="/logout" element={<Logout/>} > </Route>      

        </Routes>
      </Sidebar>
    </Router>
    </Row>
    </>
  );
}
else{
  return(
    <Router>
        <Routes>
        <Route path ="/" element={<Login acceder={acceder} setUserLogged={setUserLogged}/>}/>        </Routes>  
    </Router>

  );
  
}
};

export default App;