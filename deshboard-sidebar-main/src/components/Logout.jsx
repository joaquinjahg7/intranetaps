import React from "react";
import Modal from 'react-bootstrap/Modal'
import Button from 'react-bootstrap/Button'
import Alert from 'react-bootstrap/Alert'

const Logout = () => {
    const handleLogout = () => {
        window.localStorage.clear()
        window.location.href = "/"
    }
    const handleCancel = () => window.location.href = "/"
    return (
        <>
            <Modal   show={true} size="sg">
                <Modal.Header closeButton>
                    <Modal.Title></Modal.Title>
                </Modal.Header>

                <Modal.Body>
                        <Alert variant='info'>
                            Seguro desea Salir? 
                        </Alert>
                </Modal.Body>
                <Modal.Footer>

                        <Button onClick={handleLogout} >
                            Aceptar
                        </Button>
                        <Button variant="secondary" onClick={handleCancel}>
                            Cancelar
                        </Button>

                </Modal.Footer>
                
            </Modal>
        </>
    )

} 

export default Logout