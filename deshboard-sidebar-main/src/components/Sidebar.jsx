import React, { useState } from 'react';

import {
    FaTh,
    FaBars,
    FaUserAlt,
    FaRegChartBar,
    FaCommentAlt,
    FaShoppingBag,
    FaThList,
    FaWpforms
}from "react-icons/fa";
import { NavLink } from 'react-router-dom';


const Sidebar = ({children, usuario}) => {

    const[isOpen ,setIsOpen] = useState(false);
    const toggle = () => setIsOpen (!isOpen);

    const menuItemAdmi=[

        {
            path:"/",
            name:"Perfil",
            icon:<FaUserAlt/>
        },
        {
            path:"/Autoevaluacion",
            name:"Gestion Autoevaluación ",
            icon:<FaTh/>
        },

        {
            path:"/PostForm",
            name:"Gestión Precalifacion",
            icon:<FaTh/>
        },
    ]

    const menuItem=[

        {
            path:"/",
            name:"Perfil",
            icon:<FaUserAlt/>
        },
        {
            path:"/Autoevaluacion",
            name:"Autoevaluacion",
            icon:<FaTh/>
        },
    ]

    const menuItemJefe=[

        {
            path:"/",
            name:"Perfil",
            icon:<FaUserAlt/>
        },
        {
            path:"/Autoevaluacion ",
            name:"Autoevaluacion",
            icon:<FaTh/>
        },
        {
            path:"/Precalificacion",
            name:"Precalifacion",
            icon:<FaWpforms/>
        },
    ]

    return (
        
        <div className="container">
           <div style={{width: isOpen ? "200px" : "50px"}} className="sidebar">
               <div className="top_section">
                   <h1 style={{display: isOpen ? "block" : "none"}} className="logo">{usuario.nombre}</h1>
                   <div style={{marginLeft: isOpen ? "30px" : "-10px"}} className="bars">
                       <FaBars onClick={toggle}/>
                   </div>
               </div>

               {   
                   usuario.etiquetaTipoUsuario === "Administrador" &&
                   menuItemAdmi.map((item, index)=>(
                       <NavLink to={item.path} key={index} className="link" activeclassname="active">
                           <div className="icon">{item.icon}</div>
                           <div style={{display: isOpen ? "block" : "none"}} className="link_text">{item.name}</div>
                       </NavLink>
                   ))
                   }
               
               {   
                   usuario.etiquetaTipoUsuario === "Funcionario" &&
                   menuItem.map((item, index)=>(
                       <NavLink to={item.path} key={index} className="link" activeclassname="active">
                           <div className="icon">{item.icon}</div>
                           <div style={{display: isOpen ? "block" : "none"}} className="link_text">{item.name}</div>
                       </NavLink>
                   ))
                   }
                
                {   
                   usuario.etiquetaTipoUsuario === "JefeDirecto" &&
                   menuItemJefe.map((item, index)=>(
                       <NavLink to={item.path} key={index} className="link" activeclassname="active">
                           <div className="icon">{item.icon}</div>
                           <div style={{display: isOpen ? "block" : "none"}} className="link_text">{item.name}</div>
                       </NavLink>
                   ))
                   }

                   
                   
               
           </div>
           <main>{children}</main>
        </div>
                
    );
};

export default Sidebar;