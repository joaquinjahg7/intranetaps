import '../css/login.css';
import React, {useRef, useState} from 'react';
const URL_LOGIN = "http://localhost/ws-login/login.php";


const enviarData = async ( url, data )=> {

const resp = await   fetch (url, {
        method:'POST',
        body: JSON.stringify(data),
        headers: {
            'Content-Type': 'application/json'
        }
    } );

   // console.log(resp);
    const json = await resp.json()
    //console.log(json);

    return json;
}

export default function Login({acceder, setUserLogged}) {

        const [error, setError]= useState(null);
        const [espera, setEspera]= useState(false);

        const refUsuario = useRef(null);
        const refClave= useRef(null);

        const handleLogin= async()=> {
            setEspera(true);

            const data ={
                "usuario" : refUsuario.current.value,
                "clave": refClave.current.value
            };
            //console.log(data);
            const respuestaJson = await enviarData (URL_LOGIN, data );
            console.log("respuesta desde el evento", respuestaJson);

            acceder( respuestaJson.conectado)
            if (respuestaJson.conectado){
                window.localStorage.setItem('loggedUser',JSON.stringify(respuestaJson))
                setUserLogged({
                    nombre:respuestaJson.nombre,
                    apellidos:respuestaJson.apellidos,
                    id:respuestaJson.id,
                    idTipoUsuario:respuestaJson.idTipoUsuario,
                    jefe:respuestaJson.jefe,
                    etiquetaTipoUsuario:respuestaJson.etiquetaTipoUsuario,
                    usuario:respuestaJson.usuario
                  })
            }
            setError(respuestaJson.error) //mensaje de error desde server
            setEspera(false);

        }

    return (
        <div className="login">
            <div className="row">
                <div className="col-sm-4 offset-4 mt-5">
                    <div className="card pt-5 col-sm-8">
                        <div className="card-header text-center">
                        <h2>💼 INTRANET</h2>
                        <h3>APS SAN CLEMENTE</h3>
                        </div>
                        <div className="card-body text-center">
                            <div className="input-group mb-3">
                                <span className="input-group-text" id="basic-addon1">
                                👤
                                </span>
                                <input
                                    type="text"
                                    className="form-control"
                                    placeholder="Usuario"
                                    aria-label="Username"
                                    aria-describedby="basic-addon1"
                                    ref={refUsuario}
                                />
                            </div>

                            <div className="input-group mb-3">
                                <span className="input-group-text" id="basic-addon2">
                                🔒
                                </span>
                                <input
                                    type="password"
                                    className="form-control"
                                    placeholder="Contraseña"
                                    aria-label="contraseña"
                                    aria-describedby="basic-addon2"
                                    ref={refClave}
                                />
                            </div>
                            {
                                error && 
                                    <div className="alert alert-danger">
                                        {error}
                                    </div>
                            }
                            <button 
                                onClick={handleLogin}
                                disabled={ espera }
                                className="btn btn-info btn-lg btn-block">Acceder</button>
                        </div>
                    </div>
                </div>
            </div>
            <div className="Logo">
                <div className='col-xs-0 col-sm-0 col-md-4 col-lg-4 q-pt-lg q-pl-lg'>
                    <img src='https://intranet.saludsanclemente.cl/img/logo.2077ee8f.png'>
                    </img>
                </div>
            </div>
        </div>
    );
}